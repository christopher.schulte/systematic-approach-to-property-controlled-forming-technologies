<img src="https://spp2183.de/wp-content/uploads/2021/03/cropped-Logo-SPP2183.png" alt="drawing" width="400"/>

<div style="color: black; font-size: 2.5rem; font-weight: 390; text-align: center;">
Systematic approach to property controlled forming technologies
</div>

[Gitlab](https://git.rwth-aachen.de/spp2183/systematic-approach-to-property-controlled-forming-technologies)

![color](#D9DADA)