# Examples

We are pleased to introduce the forthcoming chapter, "Examples," within our comprehensive discourse on property-controlled forming technologies. In this segment, we endeavor to illuminate the practical applications and methodologies that epitomize the essence and promise of property-controlled forming.

Through a meticulous examination of case studies, demonstrations, and practical instances, we aim to furnish a thorough portrayal of how property-controlled forming technologies are reshaping the landscape of modern manufacturing. Each example serves as a testament to the versatility, precision, and transformative potential embedded within these pioneering processes.

Our narrative will traverse a spectrum of real-world applications, spanning additive manufacturing, precision forging, surface modification, advanced coatings, and beyond. By spotlighting these diverse contexts, we seek to underscore the profound implications of independent control over both geometry and material properties in shaping the trajectory of manufacturing innovation.

We invite you to embark on this journey of exploration, where tangible outcomes and concrete examples illuminate the transformative prowess inherent in property-controlled forming technologies. With each illustration, we aspire to engender inspiration, insight, and discernment regarding the multifaceted dimensions of this burgeoning field.

# Property-controlled cold rolling

In context of this contribution, a rolling mill from Buehler Redex GmbH, Pforzheim, Germany is considered, which was first described in [Wehr et al. (2018)](https://www.sciencedirect.com/science/article/pii/S0007850618301318) and is depicted in [Overview](#Overview-Process-Cold-Rolling). A narrow metal strip with an initial strip thickness h0, strip width w0, and roughness Ra0 is used. The strip is fed through the first roll stand with an actuated roll gap of s1, where the [rolling force F1](02_Process Variables/coldrolling_force.md) is measured. As a result, the outgoing strip thickness h1 is smaller than h0, while the strip width w1 and the strip length are increased (w1 > w0). In order to decouple the two rolling stands and to control the strip tension between the stands, a dancer is integrated into the line, which acts as a strip buffer. By measuring the strip speed upstream and downstream of the dancer and measuring the dancer angle φ, the strip length buffered in the dancer can be determined.

#### Overview Process Cold Rolling
![Overview](05_Processes/01_Cold_Rolling/Walzanlage.png)


#### Process variables
- [Force](03_Sensors/Sensors.md#load-cells): The process force measured by [...]
- incoming and outgoing thickness h0,h1,h2
- incoming and outgoing width w0,w1,w2
- Roll gap height s1, s2

#### Property
- [Roughness Ra](01_Properties/coldrolling_roughness.md)

#### Experiment 
![Experiment](05_Processes/01_Cold_Rolling/ergebnisse.png)

# Property-controlled multi-stage press hardening

Multi-stage press hardening in the progressive die enables the production of small to medium-sized high-strength components with complex geometries in large quantities. The product properties (e.g. hardness and sheet thinning) result from the thermo-mechanical history in the multi-stage die, which is subject to complex interactions. With the aim of compensating for these interactions and thus robustly controlling the product properties, the process is supplemented by a control loop.

# Example process and concept for the control loop

For the development of a property control system, multi-stage press hardening in a progressive die (see Figure), which is installed in a servo press is used as an example process. The investigated materials are 22MnB5 and X46Cr13 sheet strips with a cross-section of 2 x 200 mm. In the upstream stages, the sheet strip is first pre-cut into into rectangular blanks (W x L: 60 x 140 mm). In the first stage, the entire blank is inductively heated to the austenitizing temperature Tγ in the progressive die and held at this temperature for the time tγ. This allows the state of homogenization and thus the hardenability of the material to be adjusted. In the second stage, a temperature profile is set over the longitudinal direction of the blank, which prolongs homogenization in area 2 (A2) and also pre-controls the sheet thining in the forming stages. The temperature in area 1 (A1) can be lowered to the temperature Tcool by air cooling and raised to Theat in area 2 (A2) by resistance heating. In the third stage, the first forming stage, a hat shaped profile is stretch-formed. During stretch forming the blank holder force FBH can be adapted, whereby the sheet draw-in E and thus also the sheet thinning Δs can be modified. Finally, a frame of the hat profile is bent during die bending and the resulting profile is calibrated. During both stretch forming and die bending, the formed blank is quenched by contact with the tools during the holding phase of the ram at bottom dead center. By adjusting the stroke rate fSR of the servo press, the dwell time at bottom dead center is scaled and thus the effective quenching rate of the multi-stage process can be changed. During the process, the temperature is measured at certain points using thermocouples installed in the tools and pyrometers. In addition, the temperature distribution in the sheet is recorded during transportation with the tool open using a thermal imaging camera. The measured temperature (distribution) is evaluated by a cascade of soft sensors, which predicts the product properties based on this. The latter are compared with specified target properties so that the process control adjusts the process control variables accordingly.

![FigureMultiStage](05_Processes/02_MultiStage_PressHardening/Multistage_presshardening_240328.png)

#### Control variables
- Stage 1: austenitization temperature - Tγ, dwell time - tγ
- Stage 2: heating temperature of area 2 (A2) - Theat, cooling temperature of area 1 (A1) - Tcool
- Stage 3: blankholder force - FBH
- Stage 1-4: Stroke rate of the press - fSR

#### Property
- Hardness in HV of area 1 and 2
- Sheet thinning Δs in area 1 and 2

# Property-controlled flow forming

#### Control concept
The aim of the property control for the flow forming process is to control both the wall thickness reduction (geometry) and the α'-martensite volume fraction (property) in a multivariable control. 

![FigureFlowFormingControl](05_Processes/03_Flow_Forming/Control_Structure_flow_forming.svg 	)


#### Results

![FigureFlowFormingControl](05_Processes/03_Flow_Forming/Simulated_wall_thickness_by_closed-loop.svg)

### Other example

Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
