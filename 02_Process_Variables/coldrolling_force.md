# Force in Cold Rolling

This is an overview of the process variable "force" in the cold rolling process.

It can be measured in the roll stand of the system:

![Roll stand](../05_Processes/01_Cold_Rolling/Roll_stand.png)

